package com.zonestudio.zonetv.main;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.View;


import com.zonestudio.zonetv.R;

import java.util.ArrayList;
import java.util.List;

import br.liveo.interfaces.NavigationLiveoListener;
import br.liveo.navigationliveo.NavigationLiveo;
import io.vov.vitamio.LibsChecker;

/**
 * Created by ntb on 1/28/2015.
 */
public class Main_Activity extends NavigationLiveo implements NavigationLiveoListener {

    private List<String> listNameItem;
    private Fragment fragment;

    @Override
    public void onUserInformation() {

        this.mUserName.setText("VietTV - Tivi online for VietNamese");
        this.mUserEmail.setText("Copyright by ZoneStudio ");
        this.mUserEmail.setPadding(20,20,20,20);
        this.mUserEmail.setTextSize(12);
        this.mUserEmail.setTextColor(Color.parseColor("#F7F7F7"));
        this.mUserPhoto.setImageResource(R.mipmap.logo);
        this.mUserPhoto.setAdjustViewBounds(true);
        this.mUserBackground.setBackgroundColor(Color.parseColor("#64ce90"));
    }

    @Override
    public void onInt(Bundle savedInstanceState) {

        this.setColorSelectedItemNavigation(R.color.actionbar);
        this.setNavigationListener(this);
        this.setDefaultStartPositionNavigation(1);
        listNameItem = new ArrayList<>();
        listNameItem.add(0, "Chanel");
        listNameItem.add(1, "Live TV");
        listNameItem.add(2, "TV Show");
        listNameItem.add(3, "Musics");
        listNameItem.add(4, "Sports");
        listNameItem.add(5, "Other");
        listNameItem.add(6, "Tools");
        listNameItem.add(7, "Favourites");
        listNameItem.add(8, "History");
        listNameItem.add(9, "Share");
        listNameItem.add(10, "Setting");

        List<Integer> listIconItem = new ArrayList<>();
        listIconItem.add(0,0);
        listIconItem.add(1,R.mipmap.ic_tv_grey600_18dp);
        listIconItem.add(2,R.mipmap.ic_launcher);
        listIconItem.add(3,R.mipmap.ic_my_library_music_grey600_24dp);
        listIconItem.add(4,R.mipmap.ic_launcher);
        listIconItem.add(5,R.mipmap.ic_launcher);
        listIconItem.add(6,0);
        listIconItem.add(7,R.mipmap.ic_favorite_grey600_18dp);
        listIconItem.add(8,R.mipmap.ic_history_grey600_36dp);
        listIconItem.add(9,R.mipmap.ic_share_grey600_24dp);
        listIconItem.add(10,R.mipmap.ic_settings_grey600_36dp);

        List<Integer> listHeader = new ArrayList<>();
        listHeader.add(6);
        listHeader.add(0);

        SparseIntArray mSparseCounterItem = new SparseIntArray(); //indicate all items that have a counter
        mSparseCounterItem.put(1, 7);
        mSparseCounterItem.put(2, 10);
        mSparseCounterItem.put(3, 7);
        mSparseCounterItem.put(4, 10);
        mSparseCounterItem.put(5, 7);

        this.setNavigationAdapter(listNameItem,listIconItem,listHeader,mSparseCounterItem);

    }

    @Override
    public void onItemClickNavigation(int position, int layoutContainerId) {

        switch (position){
            case 1:
                fragment = new FragmentMain();
                break;
            case 2:
                fragment = new FragmentTvShows();
                break;
            case 3:
                fragment = new Fragment();
                break;
            case 4:
                fragment = new Fragment();
                break;
            case 5:
                fragment = new Fragment();
                break;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(layoutContainerId,fragment).commit();
    }

    @Override
    public void onPrepareOptionsMenuNavigation(Menu menu, int position, boolean visible) {

    }

    @Override
    public void onClickFooterItemNavigation(View v) {

    }

    @Override
    public void onClickUserPhotoNavigation(View v) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://zonesoft.net/"));
        startActivity(intent);
    }
}
